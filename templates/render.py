#!/usr/bin/env python3

# FIXME: add strict undefined

import itertools
from pathlib import Path

from jinja2 import Environment, FileSystemLoader
import yaml


REPO_ROOT = Path(__file__).resolve().parent.parent
TEMPLATE_FOLDER = REPO_ROOT.joinpath("templates")


# Jinja filters
def tag_list(tags):
    return " ".join(["--tag {}".format(tag) for tag in tags])


def flatten(lists):
    return itertools.chain(*lists)


# Utilities
def load_variables(file_):
    with TEMPLATE_FOLDER.joinpath(file_).open() as fp:
        return yaml.safe_load(fp)


def generate_gitlabci(env, variables):
    template = env.get_template("gitlab-ci.yml.j2")

    with REPO_ROOT.joinpath(".gitlab-ci.yml").open("w") as fp:
        fp.write(template.render(**variables))


def generate_dockerfiles(env, directory, images):
    template = env.get_template("Dockerfile.j2")

    for image_name, variables in images.items():
        if image_name.startswith("."):
            continue

        base_image = variables.get("base", image_name)
        filename = "{}/{}.Dockerfile".format(directory, image_name).replace(
            ":", "-"
        )
        with REPO_ROOT.joinpath(filename).open("w") as fp:
            fp.write(template.render(base_image=base_image, **variables))


def main():
    env = Environment(
        extensions=["jinja2.ext.do"],
        loader=FileSystemLoader(str(TEMPLATE_FOLDER)),
        lstrip_blocks=True,
        trim_blocks=True,
    )

    env.filters["tag_list"] = tag_list
    env.filters["flatten"] = flatten

    generate_gitlabci(env, load_variables("variables.yml"))
    generate_dockerfiles(
        env, "testsuite", load_variables("testsuite_images.yml")
    )
    generate_dockerfiles(
        env, "buildstream", load_variables("buildstream_images.yml")
    )


if __name__ == "__main__":
    main()
